package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import com.amdocs.Increment;

public class IncrementTest {

    @Test
    public void test1() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Add", 1, k);

    }

    @Test
    public void test2() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Add", 1, k);

    }
    @Test
    public void test3() throws Exception {

        int k= new Increment().decreasecounter(2);
        assertEquals("Add", 1, k);

    }
}

